# Bciexcercise
This is a Spring Boot project for a REST API that provides user sign-up and login functionality using an H2 in-memory database.

## Prerequisites
- Java 8 or higher

## Getting Started
Clone the repository:
`git clone https://gitlab.com/juaco1993/bciexcercise`

Build the project using Gradle
`./gradlew build`

Run and build the project using Gradle:
`./gradlew bootRun`


The API will be available at http://localhost:8080.

## API Endpoints
The following endpoints are available:

### POST /sign-up
Create a new user account.

Request Body:
```json
{
    "name": "Eduardo",
    "email": "pedro@gmail.com",
    "password": "a2asfGfdfdf4",
    "phones": [
        {
            "number": 12312313,
            "citycode": 12312313,
            "countrycode": "123"
        }
    ]
}
```
Response:
```json
{
    "id": 4202,
    "name": "Eduardo",
    "created": "2023-03-01T11:07:02.496692",
    "lastLogin": "2023-03-01T11:07:02.49671",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZWRyb0BnbWFpbC5jb20iLCJpYXQiOjE2Nzc2Nzk2MjIsImV4cCI6MTY3Nzc2NjAyMiwianRpIjoiZjI1NDk2OWItMzRkNS00YmViLTk4MWEtNTExY2QyMmE1OWY5In0.ie3_L4TrIHJCrQ6cAhhksTXyGSvF8LyCXtt3qM5eZio",
    "active": true
}
```
### POST /login
Log in to an existing user account.

Request Header:
`Authorization: Bearer <token here>`

Response:
```json
{
    "id": 4202,
    "created": "2023-03-01T00:00:00",
    "lastLogin": "2023-03-01T11:07:44.465589",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZWRyb0BnbWFpbC5jb20iLCJpYXQiOjE2Nzc2Nzk2NjQsImV4cCI6MTY3Nzc2NjA2NCwianRpIjoiOWNiNTM5NmQtMjUyMC00OTlkLWI2MjAtZWU1ZDE2MWU2NzRkIn0.UX-pTg4TkJkZsfpaLlhhhT-Zi7TlPAo_RnYfJOMENSE",
    "name": "Eduardo",
    "email": "pedro@gmail.com",
    "password": "$2a$10$lmB./.ZuAUxpk86/uQL05uqmSz5bFdTIzTSs89ydxXE9MBRMN2r82",
    "phones": null,
    "active": true
}
```


The token can be used to authenticate requests to protected endpoints.

## Security
This project uses JWT (JSON Web Tokens) for authentication and authorization. The POST /sign-up and POST /login endpoints both return a token that should be included in the Authorization header of protected requests:

`Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...`
## H2 Console
The H2 in-memory database used in this project provides a web-based console for exploring the database contents. The console is available at http://localhost:8080/h2-ui (username: sa, password: ).

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.