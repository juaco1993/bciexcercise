package com.globallogic.bciexcercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BciexcerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BciexcerciseApplication.class, args);
	}

}
