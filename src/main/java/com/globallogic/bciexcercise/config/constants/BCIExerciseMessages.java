package com.globallogic.bciexcercise.config.constants;

public class BCIExerciseMessages {
    public static final String NOT_VALID_EMAIL_MESSAGE = "El email proporcionado no es válido";
    public static final String NOT_BLANK_PASSWORD_MESSAGE = "La contrasenia es obligatoria";

    public static final String NOT_VALID_PASSWORD_MESSAGE = "La contrasenia debe tener solo una mayúscula y solamente dos números (no necesariamente " +
            "consecutivos), en combinación de letras minúsculas, largo máximo de 12 y mínimo 8.";

    public static final String EMAIL_ALREADY_IN_USE = "Error: el email ya se encuentra en uso!";

    public static final String TOKEN_NOT_VALID = "El token proporcionado no es válido";

    public static final String TOKEN_EXPIRED_OR_NOT_VALID = "El token proporcionado no es válido o ya ha expirado";

    public static final String USER_SUCCESSFULLY_REGISTERED = "Usuario registrado con éxito";
}
