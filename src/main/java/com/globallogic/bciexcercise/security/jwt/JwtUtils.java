package com.globallogic.bciexcercise.security.jwt;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;


@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${bciexercise.jwtSecret}")
    private String jwtSecret;

    @Value("${bciexercise.jwtExpirationMs}")
    private int jwtExpirationMs;

    @Value("${bciexercise.jwtCookieName}")
    private String jwtCookie;

    private Key hmacKey;

    private Key getHmacKey(){
        if(hmacKey == null){
            hmacKey = new SecretKeySpec(Base64.getDecoder().decode(jwtSecret),
                    SignatureAlgorithm.HS256.getJcaName());
        }
        return hmacKey;
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parserBuilder().setSigningKey(getHmacKey()).build().parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateJwtToken(String jwtString) {
        try{
            Jwts.parserBuilder()
                .setSigningKey(getHmacKey())
                .build()
                .parseClaimsJws(jwtString);
            return true;
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }

    public String generateTokenFromUsername(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .setId(UUID.randomUUID().toString())
                .signWith(getHmacKey())
                .compact();
    }
}
