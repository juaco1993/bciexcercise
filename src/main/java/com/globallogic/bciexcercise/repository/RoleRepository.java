package com.globallogic.bciexcercise.repository;

import com.globallogic.bciexcercise.entity.ERole;
import com.globallogic.bciexcercise.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
