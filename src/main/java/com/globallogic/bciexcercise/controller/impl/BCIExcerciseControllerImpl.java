package com.globallogic.bciexcercise.controller.impl;

import com.globallogic.bciexcercise.controller.BCIExcerciseController;
import com.globallogic.bciexcercise.dto.UserSignUpRequest;
import com.globallogic.bciexcercise.dto.UserSignUpResponse;
import com.globallogic.bciexcercise.exception.ErrorResponse;
import com.globallogic.bciexcercise.service.impl.BCIExcerciseServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
public class BCIExcerciseControllerImpl implements BCIExcerciseController {

    private static final Logger logger = LoggerFactory.getLogger(BCIExcerciseControllerImpl.class);
    private final BCIExcerciseServiceImpl service;

    @Override
    @PostMapping("/sign-up")
    public ResponseEntity<Object> userSignUp(@Valid @RequestBody UserSignUpRequest request) {
        ResponseEntity<Object> response;
        logger.info("SE RECIBIO PETICION PARA POST /SIGN-UP");
        try{
            UserSignUpResponse signUpResponse = service.performUserSignUp(request);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Authorization",signUpResponse.getToken());
            response = ResponseEntity
                    .ok()
                    .headers(responseHeaders)
                    .body(signUpResponse)
            ;
        }catch (Exception e){
            response = ResponseEntity.badRequest().body(new ErrorResponse(LocalDateTime.now(),HttpStatus.BAD_REQUEST.value(),e.getMessage()));
        }
        return response;
    }

    @Override
    @PostMapping("/login")
    public ResponseEntity<Object> userLogin() {
        logger.info("SE RECIBIO PETICION PARA POST /LOGIN");
        try{
            return ResponseEntity.ok(service.performUserLogin());
        }catch (Exception e){
            return ResponseEntity.badRequest().body(new ErrorResponse(LocalDateTime.now(),HttpStatus.BAD_REQUEST.value(),e.getMessage()));
        }
    }
}
