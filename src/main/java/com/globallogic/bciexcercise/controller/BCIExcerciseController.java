package com.globallogic.bciexcercise.controller;
import com.globallogic.bciexcercise.dto.UserSignUpRequest;
import org.springframework.http.ResponseEntity;

public interface BCIExcerciseController {
    ResponseEntity<Object> userSignUp(UserSignUpRequest request);

    ResponseEntity<Object> userLogin();
}
