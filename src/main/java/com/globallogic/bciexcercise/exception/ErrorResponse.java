package com.globallogic.bciexcercise.exception;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class ErrorResponse {
    private Set<Error> error = new HashSet<>();

    public ErrorResponse(Set<Error> error) {
        this.error = error;
    }

    public ErrorResponse(LocalDateTime timestamp, Integer codigo, String detail) {
        Error error1 = new Error(timestamp,codigo,detail);
        this.error.add(error1);
    }

    public ErrorResponse(Error error) {
        this.error.add(error);
    }

    public Set<Error> getError() {
        return error;
    }

    public void setError(Set<Error> error) {
        this.error = error;
    }
}
