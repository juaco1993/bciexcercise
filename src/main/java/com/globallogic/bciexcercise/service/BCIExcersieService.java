package com.globallogic.bciexcercise.service;

import com.globallogic.bciexcercise.dto.UserLoginResponse;
import com.globallogic.bciexcercise.dto.UserSignUpRequest;
import com.globallogic.bciexcercise.dto.UserSignUpResponse;

public interface BCIExcersieService {

    UserSignUpResponse performUserSignUp(UserSignUpRequest request);

    UserLoginResponse performUserLogin();
}
