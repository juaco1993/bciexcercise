package com.globallogic.bciexcercise.service.impl;

import com.globallogic.bciexcercise.config.constants.BCIExerciseMessages;
import com.globallogic.bciexcercise.dto.UserLoginResponse;
import com.globallogic.bciexcercise.dto.UserSignUpRequest;
import com.globallogic.bciexcercise.dto.UserSignUpResponse;
import com.globallogic.bciexcercise.entity.ERole;
import com.globallogic.bciexcercise.entity.Role;
import com.globallogic.bciexcercise.entity.User;
import com.globallogic.bciexcercise.repository.UserRepository;
import com.globallogic.bciexcercise.security.jwt.JwtUtils;
import com.globallogic.bciexcercise.service.BCIExcersieService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.SmartValidator;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class BCIExcerciseServiceImpl implements BCIExcersieService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final SmartValidator validator;

    @Override
    public UserSignUpResponse performUserSignUp(UserSignUpRequest signUpRequest) {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new RuntimeException(BCIExerciseMessages.EMAIL_ALREADY_IN_USE);
        }

        // Create User and save it to db
        User user = new User();
        user.setIsActive(true);
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(encoder.encode(signUpRequest.getPassword()));
        user.setName(signUpRequest.getName());
        user.setPhones(signUpRequest.getPhones());
        user.setCreated(LocalDateTime.now());
        user.setLastLogin(LocalDateTime.now());
        Set<Role> roles = new HashSet<>();
        Role role = new Role(ERole.ROLE_USER);
        roles.add(role);
        user.setRoles(roles);

        //Validation of the User
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        User savedUser = userRepository.save(user);

        //Construction of response
        UserSignUpResponse response = new UserSignUpResponse();
        response.setIsActive(savedUser.getIsActive());
        response.setId(savedUser.getId());
        response.setName(savedUser.getName());
        response.setCreated(savedUser.getCreated());
        response.setLastLogin(savedUser.getLastLogin());

        // Construction of token
        String token = jwtUtils.generateTokenFromUsername(signUpRequest.getEmail());
        response.setToken(token);

        return response;
    }

    @Override
    public UserLoginResponse performUserLogin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = jwtUtils.getUserNameFromJwtToken(authentication.getCredentials().toString());
        Optional<User> user = userRepository.findByEmail(email);
        UserLoginResponse userLoginResponse = new UserLoginResponse();
        if(user.isPresent()){
            User userInDB = user.get();
            userLoginResponse.setId(userInDB.getId());
            userLoginResponse.setIsActive(userInDB.getIsActive());
            userLoginResponse.setLastLogin(LocalDateTime.now());
            userLoginResponse.setCreated(userInDB.getCreated());
            userLoginResponse.setEmail(userInDB.getEmail());
            userLoginResponse.setName(userInDB.getName());
            userLoginResponse.setPassword(userInDB.getPassword());
            userLoginResponse.setPhones(userInDB.getPhones());
            userLoginResponse.setToken(jwtUtils.generateTokenFromUsername(userInDB.getEmail()));
        }else{
            throw new RuntimeException(BCIExerciseMessages.TOKEN_NOT_VALID);
        }

        return userLoginResponse;
    }

}
