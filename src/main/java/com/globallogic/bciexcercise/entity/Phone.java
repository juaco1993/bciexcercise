package com.globallogic.bciexcercise.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="phones")
@Getter @Setter @NoArgsConstructor
public class Phone {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @Column(name = "number", nullable = false)
    private Long number;

    @Column(name = "citycode", nullable = false)
    private Integer citycode;

    @Column(name = "countrycode", nullable = false)
    private String countrycode;

    public Phone(Long number, Integer citycode, String countrycode) {
        this.number = number;
        this.citycode = citycode;
        this.countrycode = countrycode;
    }
}
