package com.globallogic.bciexcercise.entity;

import com.globallogic.bciexcercise.config.constants.BCIExerciseMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
@Getter @Setter @NoArgsConstructor
public class User {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "name")
    private String name;

    @Column(name = "email", nullable = false)
    @Email
    @NotBlank(message = BCIExerciseMessages.NOT_VALID_EMAIL_MESSAGE)
    private String email;

    @Column(name = "password", nullable = false)
    //@Pattern(message = BCIExerciseMessages.NOT_VALID_PASSWORD_MESSAGE,regexp = "^(?=(?:[^A-Z]*[A-Z])?[^A-Z]*$)(?=(?:\\D*\\d){2})\\w{8,12}$")
    @NotBlank(message = BCIExerciseMessages.NOT_BLANK_PASSWORD_MESSAGE)
    private String password;

    @OneToMany(mappedBy="user", cascade = CascadeType.ALL)
    private Set<Phone> phones = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public void setPhones(Set<Phone> phones) {
        for (Phone phone : phones) {
            phone.setUser(this);
        }
        this.phones = phones;
    }

}
