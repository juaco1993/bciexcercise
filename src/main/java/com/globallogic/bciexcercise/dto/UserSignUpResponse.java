package com.globallogic.bciexcercise.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
@NoArgsConstructor
@Getter
@Setter
public class UserSignUpResponse implements Serializable {

    private Long id;
    private String name;
    private LocalDateTime created;
    private LocalDateTime lastLogin;
    private String token;
    private Boolean isActive;

}
