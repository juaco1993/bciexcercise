package com.globallogic.bciexcercise.dto;

import com.globallogic.bciexcercise.entity.Phone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;
@NoArgsConstructor
@Getter
@Setter
public class UserSignUpRequest implements Serializable {
    String name;
    String email;
    String password;
    Set<Phone> phones;

}
