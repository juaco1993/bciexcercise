package com.globallogic.bciexcercise.dto;

import com.globallogic.bciexcercise.entity.Phone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
@NoArgsConstructor @Getter @Setter
public class UserLoginResponse implements Serializable {

    private Long id;
    private LocalDateTime created;
    private LocalDateTime lastLogin;
    private String token;
    private Boolean isActive;
    private String name;
    private String email;
    private String password;
    private Set<PhoneResponse> phones = new HashSet<>();

    public void setPhones(Set<Phone> phones) {
        for (Phone phone : phones) {
            PhoneResponse phoneResponse = new PhoneResponse();
            phoneResponse.setCitycode(phone.getCitycode());
            phoneResponse.setCountrycode(phone.getCountrycode());
            phoneResponse.setNumber(phone.getNumber());
            this.phones.add(phoneResponse);
        }
    }

}
