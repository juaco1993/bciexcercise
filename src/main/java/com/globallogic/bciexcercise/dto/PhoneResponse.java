package com.globallogic.bciexcercise.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class PhoneResponse {
    private Long number;
    private Integer citycode;
    private String countrycode;

}
