package com.globallogic.bciexcercise.service.impl

import com.globallogic.bciexcercise.config.constants.BCIExerciseMessages
import com.globallogic.bciexcercise.exception.ErrorResponse
import com.globallogic.bciexcercise.dto.UserSignUpRequest
import com.globallogic.bciexcercise.dto.UserSignUpResponse
import com.globallogic.bciexcercise.entity.Phone
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class BCIExcerciseServiceImplSpecification extends Specification {

    @Autowired
    private BCIExcerciseServiceImpl service

    def "should create a new user"() {
        given:
        Phone[] phones = [
                [123456, 380, "54"]
        ]
        def request = new UserSignUpRequest(name: "jorge", email: "jorgecapo@jorge.com", password: "a2asfGfdfdf4", phones: phones)

        when:
        UserSignUpResponse response = service.performUserSignUp(request)

        then:
        response.name == request.name
    }

    def "should give an error since email is registered twice"() {
        given:
        Phone[] phones = [
                [123456, 380, "54"]
        ]
        def request = new UserSignUpRequest(name: "jorge", email: "repetido@repetido.com", password: "a2asfGfdfdf4", phones: phones)

        when:
        UserSignUpResponse response = service.performUserSignUp(request)

        then:
        response.name == request.name

        when:
        ErrorResponse response2 = service.performUserSignUp(request)

        then:
        def e = thrown(Exception)
        e.message == BCIExerciseMessages.EMAIL_ALREADY_IN_USE
    }

    def "should give an error since email is not correctly formatted"() {
        given:
        Phone[] phones = [
                [123456, 380, "54"]
        ]
        def request = new UserSignUpRequest(name: "jorge", email: "thisemailwontwork", password: "a2asfGfdfdf4", phones: phones)

        when:
        service.performUserSignUp(request)

        then:
        def e = thrown(Exception)
        e.message.contains("Could not commit JPA transaction")
    }

}
